package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/fatih/color"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func connectRun(addr string, f func(context.Context, *mongo.Client)) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(addr))
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return err
	}

	f(ctx, client)
	return nil
}

func runTest(test ConfigurationTest) {
	err := connectRun(test.ConnectionString, func(ctx context.Context, client *mongo.Client) {
		for _, permTest := range test.Tests {
			err := permTest.TestFunc(ctx, client)
			success := err == nil || !permTest.ShouldPass
			var result string
			if success {
				result = color.GreenString("CONSISTENT")
			} else {
				if err == nil {
					result = color.RedString(fmt.Sprintf("INCONSISTENT: passed but shouldn't have"))
				} else {
					result = color.RedString(fmt.Sprintf("INCONSISTENT: did not pass: %s", err))
				}
			}
			fmt.Printf("[%s]#[%s]: %s\n", test.ConfigName, permTest.TestName, result)
		}
	})
	if err != nil {
		fmt.Printf("[%s]: %s\n", test.ConfigName, color.RedString(fmt.Sprintf("error connecting to mongo: %s", err)))
	}
}
