let db = connect("mongodb://root:root@localhost:27017/admin");

db = db.getSiblingDB('testingdb');

db.createUser(
  {
    user: "reader",
    pwd: "reader",
    roles: [ { role: "read", db: "testingdb"} ],
    passwordDigestor: "server",
  }
)
