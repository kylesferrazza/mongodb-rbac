let db = connect("mongodb://root:root@localhost:27017/admin");

db = db.getSiblingDB('testingdb');

db.createUser(
  {
    user: "writer",
    pwd: "writer",
    roles: [ { role: "readWrite", db: "testingdb"} ],
    passwordDigestor: "server",
  }
)
