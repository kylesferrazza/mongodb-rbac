{
  description = "mongodb-rbac";

  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.buildGoModule {
      name = "mongodb-rbac";
      version = "0.1";
      src = ./.;
      vendorSha256 = "+XWoM2dbaWJ5qTYsR3enLN322EwcCizPFlRMruDXzP0=";
    };
    devShell.x86_64-linux = pkgs.mkShell {
      name = "adventofcode2020";
      buildInputs = with pkgs; [
        go
        gopls
      ];
    };
  };
}
