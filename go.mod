module gitlab.com/kylesferrazza/mongodb-rbac

go 1.15

require (
	github.com/fatih/color v1.10.0
	go.mongodb.org/mongo-driver v1.4.4
)
