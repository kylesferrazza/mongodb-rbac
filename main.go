package main

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type PermissionTest struct {
	TestName   string
	ShouldPass bool
	TestFunc   func(context.Context, *mongo.Client) error
}

type ConfigurationTest struct {
	ConfigName       string
	ConnectionString string
	Tests            []PermissionTest
}

func tryGetDatabaseNames(ctx context.Context, client *mongo.Client) error {
	databases, err := client.ListDatabaseNames(ctx, bson.D{})
	if err != nil {
		return err
	}

	if len(databases) > 1 {
		return nil
	}
	return errors.New("No databases found.")
}

func tryInsertOne(ctx context.Context, client *mongo.Client) error {
	database := client.Database("testingdb")
	foodsCollection := database.Collection("foods")
	_, err := foodsCollection.InsertOne(ctx, bson.D{
		{Key: "name", Value: "apple"},
		{Key: "color", Value: "red"},
	})
	if err != nil {
		return err
	}
	return nil
}

func tryDropCollection(ctx context.Context, client *mongo.Client) error {
	database := client.Database("testingdb")
	foodsCollection := database.Collection("foods")
	return foodsCollection.Drop(ctx)
}

func tryDropDatabase(ctx context.Context, client *mongo.Client) error {
	database := client.Database("testingdb")
	return database.Drop(ctx)
}

func main() {
	runTest(ConfigurationTest{
		ConfigName:       "NO-AUTH",
		ConnectionString: "mongodb://localhost:27000",
		Tests: []PermissionTest{
			// No auth check, should be able to enumerate database names.
			{TestName: "DB-NAMES", TestFunc: tryGetDatabaseNames, ShouldPass: true},

			// No auth check, should be able to insert a document.
			{TestName: "INSERT-ONE", TestFunc: tryInsertOne, ShouldPass: true},

			// No auth check, should be able to drop a collection.
			{TestName: "COLLECTION-DROP", TestFunc: tryDropCollection, ShouldPass: true},

			// No auth check, should be able to drop a database..
			{TestName: "DB-DROP", TestFunc: tryDropCollection, ShouldPass: true},
		},
	})

	runTest(ConfigurationTest{
		ConfigName:       "PASS-AUTH-MISSING",
		ConnectionString: "mongodb://localhost:27001",
		Tests: []PermissionTest{
			// Password is not present, should *not* be able to enumerate database names.
			{TestName: "DB-NAMES", TestFunc: tryGetDatabaseNames, ShouldPass: false},

			// Password is not present, should *not* be able to insert a document..
			{TestName: "INSERT-ONE", TestFunc: tryInsertOne, ShouldPass: false},

			// Password is not present, should *not* be able to drop a collection.
			{TestName: "COLLECTION-DROP", TestFunc: tryDropCollection, ShouldPass: false},

			// Password is not present, should *not* be able to drop a database.
			{TestName: "DB-DROP", TestFunc: tryDropCollection, ShouldPass: false},
		},
	})

	runTest(ConfigurationTest{
		ConfigName:       "PASS-AUTH-PRESENT",
		ConnectionString: "mongodb://root:root@localhost:27001",
		Tests: []PermissionTest{
			// Password is present, should be able to enumerate database names.
			{TestName: "DB-NAMES", TestFunc: tryGetDatabaseNames, ShouldPass: true},

			// Password is present, should be able to insert a document.
			{TestName: "INSERT-ONE", TestFunc: tryInsertOne, ShouldPass: true},

			// Password is present, should be able to drop a collection.
			{TestName: "COLLECTION-DROP", TestFunc: tryDropCollection, ShouldPass: true},

			// Password is present, should be able to drop a database.
			{TestName: "DB-DROP", TestFunc: tryDropCollection, ShouldPass: true},
		},
	})

	runTest(ConfigurationTest{
		ConfigName:       "RBAC-read",
		ConnectionString: "mongodb://reader:reader@localhost:27002/testingdb",
		Tests: []PermissionTest{
			// We only have permission for the testingdb database, should not be able to enumerate database names.
			{TestName: "DB-NAMES", TestFunc: tryGetDatabaseNames, ShouldPass: false},

			// We only have read permission for testingdb, should not be able to insert a document.
			{TestName: "INSERT-ONE", TestFunc: tryInsertOne, ShouldPass: false},

			// We only have read permission for testingdb, should not be able to drop a collection.
			{TestName: "COLLECTION-DROP", TestFunc: tryDropCollection, ShouldPass: false},

			// We only have read permission for testingdb, should not be able to drop the database.
			{TestName: "DB-DROP", TestFunc: tryDropCollection, ShouldPass: false},
		},
	})

	runTest(ConfigurationTest{
		ConfigName:       "RBAC-readwrite",
		ConnectionString: "mongodb://writer:writer@localhost:27002/testingdb",
		Tests: []PermissionTest{
			// We only have permission for the testingdb database, should not be able to enumerate database names.
			{TestName: "DB-NAMES", TestFunc: tryGetDatabaseNames, ShouldPass: false},

			// We have write permission for testingdb, should be able to insert a document.
			{TestName: "INSERT-ONE", TestFunc: tryInsertOne, ShouldPass: true},

			// We have write permission for testingdb, should be able to drop a collection.
			{TestName: "COLLECTION-DROP", TestFunc: tryDropCollection, ShouldPass: true},

			// We have write permission for testingdb, should be able to drop a database.
			{TestName: "DB-DROP", TestFunc: tryDropCollection, ShouldPass: true},
		},
	})
}
