# mongodb-rbac

Exploring access controls in MongoDB.

By default, Mongo does not enable any access control.

It can be configured to use Role-Based Access Control (RBAC).

In RBAC, each user can be configured with a set of roles.

Each role can be given access to specific databases, and operations on those databases.

# Layout

`setup.go` contains a framework for creating access control tests for a MongoDB server in Go.

`main.go` runs this framework with various tests to ensure that the configurations created by the `docker-compose` setup enforce the correct policies.

`rbac-setup` contains Javascript files that are run by the `mongo-rbac` container at initialization time.

It contains scripts to setup users and roles for RBAC to Mongo for that container.

# How to run

Start all of the Mongo instances with `docker-compose up -d`.

Once those are running, the Go test suite can be run with `go run .`.

After trying out the tests, the Mongo instances can be stopped with `docker-compose stop`, and deleted with `docker-compose down`.

# Configurations tested

`NO-AUTH`: `mongodb://localhost:27000`.
Default Mongo setup with no authentication checks.

`PASS-AUTH-MISSING`: `mongodb://localhost:27001`.
Mongo setup with password, but password not supplied for authentication.

`PASS-AUTH-PRESENT`: `mongodb://root:root@localhost:27001`.
Mongo setup with password, with password supplied for authentication.

`RBAC-read`: `mongodb://reader:reader@localhost:27002/testingdb`.
Mongo setup with RBAC, connected as a user with the `read` role.

`RBAC-readwrite`: `mongodb://writer:writer@localhost:27002/testingdb`.
Mongo setup with RBAC, connected as a user with the `readWrite` role.

# Tests performed

Each of these tests is performed with each of the connection strings above. See `main.go` for more information about which tests are expected to fail, which are expected to pass, and why.

`DB-NAMES`: attempts to enumerate all of the databases present on the server

`INSERT-ONE`: attempts to insert a single document into the `foods` collection on the `testingdb` database

`COLLECTION-DROP`: attempts to drop the `foods` collection from the `testingdb` database

`DB-DROP`: attmpts to drop the `testingdb` database

# RBAC vs ABAC

MongoDB allows for role-based access control. Users can be placed into roles, which get a set of permissions for every collection and object in a particular database.
This is generally considered "coarse-grained" access control.

With a policy like attribute-based access control (ABAC), one can define permissions and policies at a finer-grained (or context-aware) level. For example, a database system with ABAC might only allow modifying a user in the `users` collection of a database if the current logged in user is a "manager" of that user (a term that is defined by context).

These kinds of finer controls are useful, especially in enterprise settings, but Mongo does not come with a solution for them out of the box. Instead, allowing end-users to connect to the database is generally discouraged. A service can be written that parses MongoDB data and implements authentication. Once a user is authenticated to that service, the service itself can make authenticated calls to Mongo on behalf of the user. In the context of the previous example, the service can be set up to check if the current user is a "manager" of the user being modified before making a call to Mongo to modify the user in question.
